<?php
$validToken = "token";
$username = "Username";
$URL = "";

$colorPush = "#ff00ff";
$colorMergeRequestOpened = "#00ff00";
$colorMergeRequestClosed = "#ff0000";
$colorMergeRequestMerged = "#551a8b";
$defaultColor = "#ffffff";

$colorCommentIssue = "#00ff00";
$colorCommentMergeRequest = "#ff0000";
$colorCommentCommit = "#551a8b";

class Webhook {

    private $url;
    public $message;

    public function __construct($url, $username=null){
        $this->url = $url;
        $this->username = $username;
    }

    public function sendRequest($message, $attachment=false){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_VERBOSE,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->buildJsonPayload($message, $attachment));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: Graphana Webhook',
            'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode == 200 ){
            return true;
        }

        return false;

    }

    private function buildJsonPayload($message, $attachment=false){

        $json = [
            'username' => $this->username,
            'text' => $message
        ];

        if($attachment){
            $json['attachments'] = [
                [
                    'title' => (isset($attachment['title'])?$attachment['title']:null),
                    'title_link' => (isset($attachment['title_link'])?$attachment['title_link']:null),
                    'text' => (isset($attachment['text'])?$attachment['text']:null),
                    'image_url' => (isset($attachment['image_url'])?$attachment['image_url']:null),
                    'color' => (isset($attachment['color'])?$attachment['color']:"#FFFFFF"),
                ],
            ];
        }

        return json_encode($json);
    }
}

if(isset($_GET['token']) && $_GET['token'] === $validToken) {

    $infos = json_decode(file_get_contents('php://input'));

    $client = new Webhook($URL, $username);

    switch ($infos->object_kind) {
        case "push":
            $commits = "";
            for($i = 0; $i < count($infos->commits); $i++) {
                $commits .= "Commit #[" . substr($infos->commits[$i]->id, 0, 8) . "](" . $infos->commits[$i]->url . ")\n";
                $commits .= "```" . "\n";
                $commits .= "> Message: " . $infos->commits[$i]->message . "\n";
                $commits .= "> Author: " . $infos->commits[$i]->author->name . "\n";
                $commits .= "> At " . date("d/m/Y H:i:s", strtotime($infos->commits[$i]->timestamp)) . " (UTC)\n";
                $commits .= "```" . "\n\n";
            }

            $commits = substr($commits, 0, -2);

            $text =
                "PUSH: [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")" . "\n\n".
                "Branch: " . str_replace ("refs/heads/", "" , $infos->ref) ."\n" .
                "Folk: " . $infos->user_name . " <" . $infos->user_email . ">, " . $infos->user_username . "\n\n" .
                $commits . "\n";

            $client->sendRequest("", [
                'text' => $text,
                'color' => $colorPush
            ]);
            break;
        case "merge_request":
            $text =
                "MERGE REQUEST [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ") \n\n" .
                "```" . "\n" .
                "> Folk: " . $infos->user->name . "\n" .
                "> Source branch: " . $infos->object_attributes->source_branch ."\n" .
                "> Target branch: " . $infos->object_attributes->target_branch ."\n" .
                "> State: " . $infos->object_attributes->state ."\n" .
                "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                "```";

            switch ($infos->object_attributes->state) {
                case "opened":
                    $color = $colorMergeRequestOpened;
                    break;
                case "closed":
                    $color = $colorMergeRequestClosed;
                    break;
                case "merged":
                    $color = $colorMergeRequestMerged;
                    break;
                default:
                    $color = $defaultColor;
            }

            $client->sendRequest("", [
                'text' => $text,
                'color' => $color
            ]);
            break;
        case "issue":
            if($infos->object_attributes->description !== "") {
                $text =
                        "ISSUE [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")\n\n" .
                        "```" . "\n" .
                        "> Folk: " . $infos->user->name . "\n" .
                        "> State: " . $infos->object_attributes->state ."\n" .
                        "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                        "> Description: " . $infos->object_attributes->description . "\n" .
                        "```";
            } else {
                $text =
                        "ISSUE [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")\n\n" .
                        "```" . "\n" .
                        "> Folk: " . $infos->user->name . "\n" .
                        "> State: " . $infos->object_attributes->state ."\n" .
                        "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                        "```";
            }

            switch ($infos->object_attributes->state) {
                case "opened":
                    $color = $colorMergeRequestOpened;
                    break;
                case "closed":
                    $color = $colorMergeRequestClosed;
                    break;
                case "merged":
                    $color = $colorMergeRequestMerged;
                    break;
                default:
                    $color = $defaultColor;
            }

            $client->sendRequest("", [
                'text' => $text,
                'color' => $color
            ]);
            break;
        case "note":
            switch($infos->object_attributes->noteable_type) {
                case "Issue":
                    $text =
                        "COMMENT [Issue #" . $infos->issue->iid . "](" . $infos->issue->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")\n\n\n" .
                        "\t[Comment n°" . $infos->object_attributes->id . "](" . $infos->object_attributes->url . ")\n" .
                        "```" . "\n" .
                        "> Folk: " . $infos->user->name . "\n" .
                        "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                        "> Content: " . $infos->object_attributes->note .
                        "```";
                    $color = $colorCommentIssue;
                    break;
                case "MergeRequest":
                    $text =
                        "COMMENT [Merge Request #" . $infos->merge_request->iid . "](" . $infos->merge_request->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")\n\n\n" .
                        "\t[Comment n°" . $infos->object_attributes->id . "](" . $infos->object_attributes->url . ")\n" .
                        "```" . "\n" .
                        "> Folk: " . $infos->user->name . "\n" .
                        "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                        "> Scrawl: " . $infos->object_attributes->note .
                        "```";
                    $color = $colorCommentMergeRequest;
                    break;
                case "Commit":
                    $text =
                        "COMMENT [Commit #" . substr($infos->commit->id, 0, 8) . "](" . $infos->commit->url . ") on [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")\n\n\n" .
                        "\t[Comment n°" . $infos->object_attributes->id . "](" . $infos->object_attributes->url . ")\n" .
                        "```" . "\n" .
                        "> Folk: " . $infos->user->name . "\n" .
                        "> At " . date("d/m/Y H:i:s", strtotime($infos->object_attributes->updated_at)) . "\n" .
                        "> Scrawl: " . $infos->object_attributes->note .
                        "```";
                    $color = $colorCommentCommit;
                    break;
            }

            $client->sendRequest("", [
                'text' => $text,
                'color' => $color
            ]);
            break;
        case "wiki_page":
            switch($infos->object_attributes->action) {
                case "create":
                    $text =
                        "WIKI [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")" . "\n\n" .
                        $infos->user->name . " created [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ")" . "\n" .
                        "Date: " . date("d/m/Y H:i:s", time());
                    break;
                case "update":
                    $text =
                        "WIKI [" . $infos->project->path_with_namespace . "](" . $infos->project->web_url . ")" . "\n\n" .
                        $infos->user->name . " updated [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ")" . "\n" .
                        "Date: " . date("d/m/Y H:i:s", time());
                    break;
                case "delete":
                    $text =
                        "[WIKI]" . "\n\n" .
                        $infos->user->name . " deleted [" . $infos->object_attributes->title . "](" . $infos->object_attributes->url . ")" . "\n" .
                        "Date: " . date("d/m/Y H:i:s", time());
                    break;
                default:
                    break;
            }
            $client->sendRequest("", [
                'text' => $text,
                'color' => $defaultColor
            ]);
            break;
    }
}