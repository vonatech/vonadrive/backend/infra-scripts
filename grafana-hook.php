<?php

class Webhook {

    private $url;
    public $message;

    public function __construct($url, $username=null){
        $this->url = $url;
        $this->username = $username;
    }

    public function sendRequest($message, $attachment=false){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_VERBOSE,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->buildJsonPayload($message, $attachment));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User-Agent: Graphana Webhook',
            'Content-Type: application/json',
        ));
        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode == 200 ){
            return true;
        }

        return false;

    }

    private function buildJsonPayload($message, $attachment=false){

        $json = [
            'username' => $this->username,
            'text' => $message
        ];

        if($attachment){
            $json['attachments'] = [
                [
                    'title' => (isset($attachment['title'])?$attachment['title']:null),
                    'title_link' => (isset($attachment['title_link'])?$attachment['title_link']:null),
                    'text' => (isset($attachment['text'])?$attachment['text']:null),
                    'image_url' => (isset($attachment['image_url'])?$attachment['image_url']:null),
                    'color' => (isset($attachment['color'])?$attachment['color']:"#FFFFFF"),
                ],
            ];
        }

        return json_encode($json);
    }
}

$validToken = "";
$URL = "";
$username = "Username";
$colorAlert = "#FF0000";
$colorOkay = "#00FF00";

if(isset($_GET['token']) && $_GET['token'] === $validToken) {

    $infos = json_decode(file_get_contents('php://input'));

    $client = new Webhook($URL, $username);

    if(isset($infos->ruleName) &&
        isset($infos->ruleUrl) &&
        isset($infos->state) &&
        isset($infos->title) &&
        isset($infos->message)) {
        if($infos->state === "ok") {

            $text =
                $infos->title . "\n" .
                $infos->ruleName . " is OK" . "\n" .
                "Message: " . $infos->message;

            $client->sendRequest("", [
                'text' => $text,
                'color' => $colorOkay
            ]);
        } elseif($infos->state === "alerting") {

            if(count($infos->evalMatches) > 0) {
                $metrics = "";
                for($i = 0; $i < count($infos->evalMatches); $i++) {
                    $metrics .= $infos->evalMatches[$i]->metric . ": " . $infos->evalMatches[$i]->value . "\n";
                }

                $text =
                    $infos->title . "\n" .
                    $infos->ruleName . " is CRITICAL" . "\n" .
                    "Message: " . $infos->message . "\n" .
                    "URL: " . $infos->ruleUrl . "\n\n" .
                    "Metrics:" . "\n" .
                    $metrics;
            } else {
                $text =
                    $infos->title . "\n" .
                    $infos->ruleName . " is CRITICAL" . "\n" .
                    "Message: " . $infos->message . "\n" .
                    "URL: " . $infos->ruleUrl . "\n";
            }

            $client->sendRequest("", [
                'text' => $text,
                'color' => $colorAlert
            ]);
        }
    }
}
